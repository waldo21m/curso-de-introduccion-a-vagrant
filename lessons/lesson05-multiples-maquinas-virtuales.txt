Múltiples máquinas virtuales

Para definir múltiples máquinas virtuales podemos seguir realizandolo desde
nuestro archivo de Vagrantfile. Para este ejemplo crearemos dos máquinas
virtuales donde tendran su propia ip privada.

    config.vm.define "host_1" do |host|
        # Esta seria una de las máquinas virtuales, si queremos que sea
        # con otra caja, lo colocamos como la siguiente línea de código
        # host.vm.box = "ubuntu/trusty64"
        host.vm.network "private_network", ip: "10.0.0.100"
    end

    config.vm.define "host_2" do |host|
        host.vm.network "private_network", ip: "10.0.0.200"
    end

Una vez realizada nuestra configuración, ejecutamos vagrant up. Si entramos
en nuestro programa virtualbox, podemos darnos cuenta que tendremos ahora
dos máquinas virtuales. Pero si ejecutamos vagrant ssh podemos darnos
cuenta que tendremos un error porque tendremos ahora varias máquinas virtuales:
This command requires a specific VM name to target in a multi-VM environment.

Con el comando 

vagrant status

Podemos ver una información acerca de nuestras máquinas virtuales donde se nos
indica el nombre y el proveedor que las está ejecutando. Para poder hacer ssh
debemos ejecutar

vagrant ssh NOMBRE_DEL_HOST
Ejemplo:
vagrant ssh host_1

Aquí lo importante es recordar acerca de las configuraciones locales y las 
configuraciones globales.